<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs = array(
    'News' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List News', 'url' => array('index')),
    array('label' => 'Create News', 'url' => array('create')),
    array('label' => 'Update News', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete News', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage News', 'url' => array('admin')),
);
?>

<h1>View News #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'caption',
        'text',
    ),
)); ?>

<h3>Комментарии:</h3>

<div id="comments"></div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/comment.jquery.js" ></script>
<script>

    $( document ).ready(function() {
        $('#comments').comments({
            debug:true,
            key:{
                model: '<?php echo $model->tableName();?>',
                model_id: <?php echo $model->id;?>
            },
            secureCode:''
        });
    });


</script>


