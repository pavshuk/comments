<?php
//phpd restart --verbose-tty=1 --auto-reload=5s

namespace phpDaemon;


use PHPDaemon\Core\Daemon;
use PHPDaemon\Core\Timer;

class Comments extends \PHPDaemon\Core\AppInstance {

    /**
     * Called when the worker is ready to go.
     * @return void
     */
    public function onReady() {
        $appInstance = $this; // a reference to this application instance for ExampleWebSocketRoute
        // URI /exampleApp should be handled by ExampleWebSocketRoute
        \PHPDaemon\Servers\WebSocket\Pool::getInstance()->addRoute('getComments', function ($client) use ($appInstance) {
            Daemon::log(1);
            return new WebSocketCommentSession($client, $appInstance);

        });
    }

}

class WebSocketCommentSession extends \PHPDaemon\WebSocket\Route {

    /**
     * Called when the connection is handshaked.
     * @return void
     */
    public function onHandshake() {
        Daemon::log("hand shake");
        $this->client->onSessionStart(function ($event) {
            if (!isset($this->client->session['counter'])) {
                $this->client->session['counter'] = 0;
            }
            ++$this->client->session['counter'];
            $this->client->sendFrame('counter in session = ' . $this->client->session['counter']);
            $this->client->sessionCommit();
            Daemon::log("af");
        });
    }

    /**
     * Called when new frame received.
     * @param string  Frame's contents.
     * @param integer Frame's type.
     * @return void
     */
    public function onFrame($data, $type) {
        Daemon::log('onFrame');
        if ($data === 'ping') {
            $this->client->sendFrame('pong', 'STRING',
                function ($client) { // optional. called when the frame is transmitted to the client
                    \PHPDaemon\Core\Daemon::log('ExampleWebSocket: \'pong\' received by client.');
                }
            );
            throw new \Exception;
        }
    }

    /**
     * Uncaught exception handler
     * @param $e
     * @return boolean Handled?
     */
    public function handleException($e) {
        $this->client->sendFrame('pong from exception');
        return true;
    }
}