<?php

class m131113_141146_news extends CDbMigration
{
	public function up()
	{
        $this->createTable("news", array(
            "id" => "pk",
            "caption" => "CHAR(30) NOT NULL",
            "text" => "TEXT NOT NULL"
        ));
	}

	public function down()
	{
		echo "m131113_141146_news does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}