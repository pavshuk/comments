<?php

class m131113_145405_addCommentToNews extends CDbMigration
{
	public function up()
	{
        $this->addColumn("news", "comments", "TEXT NOT NULL DEFAULT '[]'");
	}

	public function down()
	{
		echo "m131113_145405_addCommentToNews does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}