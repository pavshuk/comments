module.exports = (db)->
  collection = db.collection("comments")

  #//TODO make escaping forany comment structure
  escapeNewComment = (comment)->
    htmlEscape = (text) ->
      return text?.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;')

    comment.text = htmlEscape(comment.text);
    comment.user.name =htmlEscape(comment.user.name);
    comment.user.email =htmlEscape(comment.user.email);

  rez =
    getAllComments: (ws, callback) ->
      collection.findOne(ws.objectForCommentKey, (err, obj)->
        callback err if err
        if obj
          callback null, obj.subComments
        else
          callback null, []
      )


    addNewComment: (ws, params, callback)->

      #//TODO prevent save empty comment
      escapeNewComment(params.comment)

      collection.findOne(ws.objectForCommentKey, (err,obj)->
        throw err if err # //TODO return error command
        curpos = obj



        writeCallback = (err)->
          return callback( 'ошибка записи в БД') if err
          callback null, params.comment, params.path

        wrapNewComment = ( com )->
          {
            comment: params.comment,
            subComments:[]
          }

        if curpos
          curpos = curpos.subComments[i] for i in params.path
          curpos.subComments.push( wrapNewComment(params.comment) )
          collection.update({_id:obj._id},obj,{upsert:true}, writeCallback)
        else
          curpos = {
            model:ws.objectForCommentKey.model
            model_id : ws.objectForCommentKey.model_id
            subComments :[ wrapNewComment(params.comment)]
          }
          collection.insert(curpos, writeCallback)
      )

  return rez