config = require('./config')

webSocket = require('ws').Server
webSocketServer = new webSocket(config.get('daemon:webSocketServer'))
mongoDb = require('mongodb').MongoClient

log = (text)->
  console.log '['+(new Date()).toLocaleTimeString()+'] '+text

mongoDb.connect config.get('mongodb:uri'), (err, db) ->
  throw err if err

  webSocketServer.broadcast = (data, key) ->
    for i of @clients
      clKey = @clients[i].objectForCommentKey
      if clKey.model == key.model and clKey.model_id == key.model_id
        @clients[i].send data


  controller = require('./Controller')(db, webSocketServer)
  console.dir(controller)

  webSocketServer.on('connection', (ws)->
    ws.on 'message', (data, type)->
      log 'Got : ' + data
      obj = JSON.parse(data)
      if obj.command and obj.params and  controller[obj.command]
        controller[obj.command](ws, obj.params)
      else
        log 'Got undefined command from client : ' + data

    ws.on 'close', ->
      log 'CDisconnect'

    log 'New connection'
  )

